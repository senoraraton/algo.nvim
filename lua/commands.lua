local M = {}
local repoUtils = require("repo-utils")
local results = require("results")

M.lang_table = {}

function M.AlgoNvimUpdate(args)
	local params = vim.split(args.args, " ")
	if not #M.lang_table > 0 then
		error("No languages defined, please set some in your configuration.")
	end
	if #params == 1 then
		repoUtils.update_repo(M.lang_table)
	else
		table.remove(params, 1) --Nvim passes CMD name as argument 1
		local valid_params = true
		for _, lang in pairs(params) do
			if not vim.tbl_contains(M.lang_table, lang) then
				print(
					string.format("%s language is not included in language table. Please add it to your config.", lang)
				)
				valid_params = false
			end
		end
		if valid_params then
			repoUtils.update_repo(params)
		end
	end
end

function M.AlgoNvimOpen()
	if vim.b.problem then
		local libPath = string.format("%s-runner", vim.b.problem.lang)
		local success, runner = pcall(require, libPath)
		if success and runner and type(runner.compileAndExecute) == "function" then
			local bufnr = vim.api.nvim_get_current_buf()
			local content = vim.api.nvim_buf_get_lines(bufnr, 0, -1, false)
			local test_results = runner.compileAndExecute(vim.b.problem.path, table.concat(content, "\n"))
			results.renderTests(test_results)
		else
			print("Failed to load or find compileAndExecute in module.")
		end
	else
		local success, picker = pcall(require, "pickers")
		if success then
			picker.language_picker()
		else
			print("Unable to load language picker, please ensure paths.")
		end
	end
end

return M
