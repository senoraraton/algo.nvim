--pickers.lua

local M = {}

local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local previewers = require("telescope.previewers")
local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")
local scan = require("plenary.scandir")
local Path = require("plenary.path")

local AlgoNvimCore = require("init")
local problemParser = require("parser")

local function scan_problems_directory(dir)
	local opts = {
		hidden = false,
		depth = 1,
	}

	local files = scan.scan_dir(dir, opts)

	if #files == 0 then
		print("No files found in " .. dir .. " directory or directory does not exist.")
	end
	return files
end

local function isComplete(problem_path, lang)
	local problem_name = problem_path:match(".*/([^/]+)$")
	local dir_path = AlgoNvimCore.persistence_dir .. lang .. "/solutions/" .. problem_name
	local files = scan.scan_dir(dir_path, { hidden = false, only_dirs = false })

	local count = 0
	local hasTmp = false
	local completed = false
	for _, file in pairs(files) do
		local filename = Path:new(file):make_relative(dir_path)
		if filename:match("^tmp_") then
			hasTmp = true
		else
			count = count + 1
		end
	end
	if count > 0 then
		completed = true
	end
	return hasTmp, completed
end

local function build_problems_table(path)
	local problems_table = {}

	local problems = scan_problems_directory(path)
	for idx, problem_path in pairs(problems) do
		local content = problemParser.parseTestFile(problem_path)
		local chunks = problemParser.chunkTestFile(content)
		local lang = path:match("problems/(.*)/problems")
		local problemStatement = problemParser.fetchProblemStatement(content)
		local tags = problemParser.fetchTags(chunks["Tags"])
		local boilerplate = problemParser.fetchBoilerplate(content)
		local hasTmp, complete = isComplete(problem_path, lang)
		problems_table[idx] = {
			path = problem_path,
			tags = tags,
			language = lang,
			problemStatement = problemStatement,
			boilerplate = boilerplate,
			complete = complete,
			hasTmp = hasTmp,
			valid = not complete,
		}
	end
	return problems_table
end

local function build_tags_table(problems_table)
	local tags_table = {}
	local seen_tags = {}
	for _, problem in pairs(problems_table) do
		if type(problem.tags) == "table" then
			for _, tag in pairs(problem.tags) do
				if not seen_tags[tag] then
					seen_tags[tag] = true
					table.insert(tags_table, tag)
				end
			end
		end
	end
	return tags_table
end

local function generate_tmp_solution_file(content, lang, problem_path)
	local problem_name = problem_path:match(".*/([^/]+)$")
	local timestamp = os.date("%m-%d-%Y-%H:%M:%S")
	local filename = "tmp_" .. timestamp .. "-" .. problem_name .. "." .. lang
	local dir_path = AlgoNvimCore.persistence_dir .. lang .. "/solutions/" .. problem_name

	local p = Path:new(dir_path)
	if not p:exists() then
		p:mkdir({ parents = true })
	end

	local file_path = dir_path .. "/" .. filename

	local file = io.open(file_path, "w")
	if file then
		file:write(content)
		file:close()
	end
	return file_path
end

local function tag_reduce_problems_table(problems_table, tag)
	local tagged_problems = {}

	for _, problem in pairs(problems_table) do
		if type(problem.tags) == "table" then
			for _, problem_tag in pairs(problem.tags) do
				if problem_tag == tag then
					table.insert(tagged_problems, problem)
					break
				end
			end
		end
	end
	return tagged_problems
end

local function create_problem_finder(problems_table)
	return finders.new_table({
		results = problems_table,
		entry_maker = function(entry)
			return {
				value = entry,
				display = entry.path:match(".*/(.*)"),
				ordinal = entry,
				path = entry.path,
				valid = entry.valid,
			}
		end,
	})
end

local function create_language_finder()
	return finders.new_table({
		results = AlgoNvimCore.lang_table,
		entry_maker = function(entry)
			return {
				value = entry,
				display = entry,
				ordinal = entry,
				path = "problems/" .. entry .. "/problems",
			}
		end,
	})
end

local function create_tags_finder(tags_table)
	return finders.new_table({
		results = tags_table,
		entry_maker = function(entry)
			return {
				value = entry,
				display = entry:match("#(.*)"),
				ordinal = entry,
			}
		end,
	})
end

local problem_previewer = previewers.new_buffer_previewer({
	define_preview = function(self, entry)
		local lines = {}
		table.insert(lines, entry.value.problemStatement)
		vim.api.nvim_buf_set_lines(self.state.bufnr, 0, -1, false, lines)
	end,
})

local function check_tmp_file(selection)
	local problem_name = selection.path:match(".*/([^/]+)$")
	local dir_path = AlgoNvimCore.persistence_dir .. selection.language .. "/solutions/" .. problem_name
	local files = scan.scan_dir(dir_path, { hidden = false, only_dirs = false })
	if #files ~= 0 then
		for _, file in pairs(files) do
			local filename = file:match("^.*/(.*)$")
			if filename:match("^tmp_") then
				local answer = vim.fn.input("Would you like to load your previous attempt? (y/n): ")
				if answer:lower() == "y" then
					return Path:new(dir_path):joinpath(filename):absolute()
				elseif answer:lower() == "n" then
					os.remove(Path:new(dir_path):joinpath(filename):absolute())
					return generate_tmp_solution_file(selection.boilerplate, selection.language, selection.path)
				else
					check_tmp_file()
				end
			end
		end
	else
		return generate_tmp_solution_file(selection.boilerplate, selection.language, selection.path)
	end
end

local function problem_selector(prompt_bufnr, selection)
	actions.close(prompt_bufnr)

	local tmp_file = check_tmp_file(selection)

	vim.cmd("edit" .. tmp_file)
	local left_bufnr = vim.api.nvim_get_current_buf()

	local right_bufnr = vim.api.nvim_create_buf(false, true)
	vim.api.nvim_buf_set_lines(right_bufnr, 0, -1, false, vim.split(selection.problemStatement, "\n"))
	local right_win = vim.api.nvim_open_win(right_bufnr, false, {
		split = "right",
		width = math.floor(vim.api.nvim_get_option_value("columns", {}) * 0.33),
		win = 0,
	})
	vim.api.nvim_set_option_value("wrap", true, { win = right_win })

	vim.api.nvim_buf_set_var(left_bufnr, "problem", { path = selection["path"], lang = selection["language"] })
end

local function problem_picker(problem_table, opts)
	opts = opts or {}

	pickers
		.new({}, {
			prompt_title = "Select a Problem",
			finder = create_problem_finder(problem_table),
			previewer = problem_previewer,
			attach_mappings = function(prompt_bufnr, map)
				map("i", AlgoNvimCore.keybind_select_default, function()
					local selection = action_state.get_selected_entry()
					problem_selector(prompt_bufnr, selection.value)
				end)
				map("i", AlgoNvimCore.keybind_toggle_completed, function()
					for _, problem in pairs(problem_table) do
						if problem.complete == true then
							problem.valid = not problem.valid
						end
					end
					problem_picker(problem_table)
				end)
				return true
			end,
		})
		:find()
end

local function tags_picker(tags_table, problems_table, opts)
	opts = opts or {}
	pickers
		.new({}, {
			prompt_title = "Select a category",
			finder = create_tags_finder(tags_table),
			attach_mappings = function(prompt_bufnr, map)
				map("i", AlgoNvimCore.keybind_select_default, function()
					local selection = action_state.get_selected_entry()
					local tagged_problems_table = tag_reduce_problems_table(problems_table, selection.value)
					problem_picker(tagged_problems_table)
				end)
				map("i", AlgoNvimCore.keybind_select_random, function()
					local random_index = math.random(1, #problems_table)
					problem_selector(prompt_bufnr, problems_table[random_index])
				end)
				return true
			end,
		})
		:find()
end

function M.language_picker(opts)
	opts = opts or {}
	pickers
		.new({}, {
			prompt_title = "Select a language",
			finder = create_language_finder(),
			attach_mappings = function(prompt_bufnr, map)
				map("i", AlgoNvimCore.keybind_select_default, function()
					--Select from Tags
					local selection = action_state.get_selected_entry()
					local problems_table = build_problems_table(selection.path)
					local tags_table = build_tags_table(problems_table)
					tags_picker(tags_table, problems_table)
				end)
				map("i", AlgoNvimCore.keybind_select_all, function()
					--Select all from lang
					local selection = action_state.get_selected_entry()
					local problems_table = build_problems_table(selection.path)
					problem_picker(problems_table)
				end)
				map("i", AlgoNvimCore.keybind_select_random, function()
					--Select random problem from lang
					local selection = action_state.get_selected_entry()
					local problems = build_problems_table(selection.path)
					local random_index = math.random(1, #problems)
					problem_selector(prompt_bufnr, problems[random_index])
				end)
				return true
			end,
		})
		:find()
end

return M
