local M = {}

function M.parseTestFile(filepath)
	local path = filepath
	local file = io.open(path, "r")
	if not file then
		error("Unable to open file: " .. path)
	end

	local content = file:read("*all")
	file:close()

	return content
end

function M.chunkTestFile(content)
	local testChunks = {}
	local headerPattern = "^%s*(.-):%s*$"
	local currentHeader = nil
	local currentBlock = {}

	for line in content:gmatch("[^\r\n]+") do
		local header = line:match(headerPattern)
		if header then
			if currentHeader then
				testChunks[currentHeader] = table.concat(currentBlock, "\n")
			end

			currentHeader = header
			currentBlock = {}
		else
			table.insert(currentBlock, line)
		end
	end
	if currentHeader then
		testChunks[currentHeader] = table.concat(currentBlock, "\n")
	end
	return testChunks
end

function M.extractStructs(content, headerMatch)
	local typedefs = {}
	local structTypeDef = {}
	local isStruct = false

	for line in content:gmatch("[^\r\n]+") do
		if line:match("^" .. headerMatch .. ":") then
		elseif line:match("^typedef struct") then
			structTypeDef = {}
			isStruct = true
		elseif isStruct and line:match("^%s*}$") then
			table.insert(typedefs, structTypeDef)
			structTypeDef = {}
			isStruct = false
		elseif isStruct then
			local key, value = line:match("%S+%s+(.+)")
			if key and value then
				structTypeDef[key] = value
			end
		else
			local key, value = line:match("^%s*(%S+)%s*([^;%s]+)")
			if key and value then
				table.insert(typedefs, { [key] = value })
			else
				print("Failed to match standalone field key/value.")
			end
		end
	end
	return typedefs
end

function M.chunkBoilerplate(boilerplate)
	local includesTable = {}
	local paramsTable = {}
	local returnType, funcName, params, body = nil, nil, nil, nil
	local hasHeader = false

	for line in boilerplate:gmatch("[^\r\n]+") do
		if line:match("%s*#include%s+<[^>]+>") then
			table.insert(includesTable, line)
		elseif not hasHeader and line:match("^(%w+)%s+([%w_]+)%s*%(([^)]*)%)") then
			returnType, funcName, params = line:match("^(%w+)%s+([%w_]+)%s*%(([^)]*)%)")
			for param in params:gmatch("[^,]+") do
				table.insert(paramsTable, param:match("^%s*(.-)%s*$")) -- Trim any leading/trailing whitespace
			end
			hasHeader = true
			body = ""
		elseif hasHeader then
			body = body .. line .. "\n"
		end
	end

	return {
		includesTable = includesTable,
		paramsTable = paramsTable,
		returnType = returnType,
		funcName = funcName,
		body = body,
	}
end

function M.fetchTags(content)
	local tags = {}
	for tag in content:gmatch("#%S+") do
		table.insert(tags, tag)
	end
	return tags
end

function M.fetchProblemStatement(content)
	return content:match("Problem Statement:%s*(.-)%s*Boilerplate:")
end

function M.fetchBoilerplate(content)
	--This is bad, what if {{}}. Anchor to following field when solidified.
	return content:match("Boilerplate:\n(.-})")
end

function M.fetchInputStruct(content)
	return M.extractStructs(content, "Input")
end

function M.fetchReturnStruct(content)
	return M.extractStructs(content, "Return")
end

function M.fetchTestCases(content)
	local testTable = {}
	for input, output in content:gmatch("%(([^()]+)%)%s*/%s*%(([^()]+)%)") do
		local tempInputs = {}
		local tempOutputs = {}
		for num in input:gmatch("[^,]+") do
			table.insert(tempInputs, num:match("^%s*(.-)%s*$")) -- Trim any leading/trailing whitespace
		end
		for num in output:gmatch("[^,]+") do
			table.insert(tempOutputs, num:match("^%s*(.-)%s*$")) -- Trim any leading/trailing whitespace
		end
		table.insert(testTable, { inputs = tempInputs, outputs = tempOutputs })
	end
	return testTable
end

return M
