local i = require("init")

i.setup({
	lang_table = { "c", "golang", "typescript", "lua" },
})
