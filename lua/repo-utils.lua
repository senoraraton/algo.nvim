local M = {}

local function initalize_submodules(lang_table)
	local handle = io.open(".gitmodules", "r")
	if not handle then
		error("Failed to open .gitmodules, check your path")
	end
	local content = handle:read("*a")
	handle:close()

	local submodule_paths = {}

	for _, lang in pairs(lang_table) do
		local pattern = '%[submodule "' .. lang .. '"%]%s+path = ([^\n]+)'
		local path = content:match(pattern)

		if not path then
			error("Unable to find submodule path for " .. lang .. " in .gitmodules")
		else
			local cmd = string.format("git submodule update --init --recursive %s", path)
			local cmd_handle = io.popen(cmd)
			if not cmd_handle then
				error("Failed to execute command for submodule " .. lang .. " at path: " .. path)
			else
				cmd_handle:close()
			end
		end
		submodule_paths[lang] = path
	end
	return submodule_paths
end

local function check_remote_updates(paths_table)
	local repos_to_update = {}

	for lang, path in pairs(paths_table) do
		local cmd = string.format("git -C %s fetch origin && git -C %s status -uno", path, path)
		local handle = io.popen(cmd)
		if not handle then
			error("Failed to execute git fetch command from remote.")
		else
			local result = handle:read("*a")
			handle:close()
			if result then
				if result:match("Your branch is behind") then
					repos_to_update[lang] = path
				end
			else
				error("Failed to read git status output")
			end
		end
	end
	return repos_to_update
end

local function is_repo_clean(paths_table)
	local dirty_repos = {}

	for lang, path in pairs(paths_table) do
		local cmd = string.format("git -C %s status --porcelain", path)
		local handle = io.popen(cmd)
		if handle then
			local result = handle:read("*a")
			handle:close()
			if result and result ~= "" then
				table.insert(dirty_repos, lang)
			elseif not result then
				error("Failed to read Git status output")
			end
		else
			error("Unable to query status of local repo. Check paths.")
		end
	end

	if #dirty_repos > 0 then
		return "The following repos are dirty: " .. table.concat(dirty_repos, ", ")
	else
		return "clean"
	end
end

local function get_current_branch(repo_path)
	local cmd = string.format("git -C %s rev-parse --abbrev-ref HEAD", repo_path)
	local handle = io.popen(cmd)

	if not handle then
		error("Failed to open Git command handle.")
	end

	local branch = handle:read("*a"):gsub("%s+", "")
	handle:close()

	if not branch or branch == "" then
		error("Failed to get current branch name")
	end

	return branch
end

local function fetch_remote(update_table)
	for lang, path in pairs(update_table) do
		local branch = get_current_branch(path)
		local cmd = string.format("git -C %s pull origin %s", path, branch)
		local handle = io.popen(cmd)
		if not handle then
			error("Failed to execute git pull command for repo:", lang)
		else
			local result = handle:read("*a")
			handle:close()
			print(string.format("Updated %s: %s", lang, result))
		end
	end
end

function M.update_repo(langs_table)
	local submodule_paths = initalize_submodules(langs_table)
	local repo_clean = is_repo_clean(submodule_paths)
	if is_repo_clean(submodule_paths) ~= "clean" then
		print(repo_clean)
		return
	end
	local update_repos = check_remote_updates(submodule_paths)
	fetch_remote(update_repos)
end

return M
