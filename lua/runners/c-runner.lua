local M = {}
-- Load LuaJIT ffi
local ffi = require("ffi")
local parser = require("parser")

function M.testCase()
	print("HTP")
end

local function runTests(lib, entrypoint, tests_table)
	local checks = {}
	for index, test in pairs(tests_table) do
		local inputArgs = {}
		for _, input in pairs(test.inputs) do
			table.insert(inputArgs, tonumber(input))
		end
		if lib[entrypoint] then
			local results = { lib[entrypoint](unpack(inputArgs)) }
			local isMatch = true
			local errorMsg = ""

			if #results ~= #test.outputs then
				isMatch = false
				errorMsg = "Number of results does not match the expected number of outputs."
			else
				for i = 1, #results do
					if tonumber(results[i]) ~= tonumber(test.outputs[i]) then
						isMatch = false
						errorMsg = string.format("expected %s, got %s", test.outputs[i], results[i])
					end
				end
			end
			checks[index] = { isMatch = isMatch, error = errorMsg }
		else
			print("Entrypoint function not found in shared library:", entrypoint)
		end
	end
	return checks
end

function M.compileAndExecute(path, code)
	local content = parser.parseTestFile(path)
	local chunks = parser.chunkTestFile(content)
	local boilerplate = parser.chunkBoilerplate(chunks["Boilerplate"])
	for i, v in pairs(boilerplate["includesTable"]) do
		print(i, v)
	end
	local tests_table = parser.fetchTestCases(chunks["Test Cases"])
	local paramsString = ""
	local includeString = ""

	for idx, param in pairs(boilerplate["paramsTable"]) do
		paramsString = paramsString .. param
		if idx < #boilerplate["paramsTable"] then
			paramsString = paramsString .. ", "
		end
	end

	ffi.cdef(string.format(
		[[
        %s %s(%s);
    ]],
		boilerplate["returnType"],
		boilerplate["funcName"],
		paramsString
	))

	for _, include in pairs(boilerplate["includesTable"]) do
		includeString = includeString .. include .. "\n"
	end

	local cFileContent = string.format(
		[[
        %s
    ]],
		code
	)

	local filename = os.tmpname()
	local sharedLibName = filename .. ".so"

	local file = io.open(filename, "w")

	if not file then
		print("Failed to create C file:", filename)
		return
	end

	file:write(cFileContent)
	file:close()

	local compileCmd = string.format("gcc -Wall -fPIC -shared -x c -o %s %s 2>&1", sharedLibName, filename)

	local handle = io.popen(compileCmd, "r")
	if not handle then
		print("Failed to execute command:" .. compileCmd)
		return false
	end
	local cmd_output = handle:read("*a")
	handle:close()

	if cmd_output and cmd_output ~= "" then
		print("Command output:\n" .. cmd_output)
		return { cmd_output = cmd_output, test_results = nil }
	end

	local lib = ffi.load(sharedLibName)
	if not lib then
		print("Failed to load compiled function.")
		return
	end

	local test_results = runTests(lib, boilerplate["funcName"], tests_table)

	os.remove(filename)
	os.remove(sharedLibName)

	return { cmd_output = nil, test_results = test_results }
end

return M
