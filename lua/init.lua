local M = {}

local cmd = require("commands")
local repo_utils = require("repo-utils")

function M.setup(opts)
	opts = opts or {}
	if not opts.lang_table then
		print("No problem languages set.  Please add 1 or more to your AlgoNvim.setup()")
		M.lang_table = {}
	else
		M.lang_table = opts.lang_table
		local directories = {}
		local handle = vim.loop.fs_scandir("./problems")

		if handle then
			while true do
				local name, type = vim.loop.fs_scandir_next(handle)
				if not name then
					break
				end
				if type == "directory" then
					table.insert(directories, name)
				end
			end
		end

		for _, lang in pairs(M.lang_table) do
			local found = false
			for _, dir_name in pairs(directories) do
				if lang == dir_name then
					found = true
					break
				end
			end
			if not found then
				print("Error Language '" .. lang .. "' in setup not found in submodules directory.")
			end
		end
	end

	cmd.lang_table = M.lang_table
	M.keybind_launch = opts.keybind_launch or "<F12>"
	M.keybind_toggle_completed = opts.keybind_toggle or "<A-CR>"
	M.keybind_select_default = opts.keybind_select_default or "<CR>"
	M.keybind_select_all = opts.keybind_select_all or "<S-CR>"
	M.keybind_select_random = opts.keybind_select_random or "<C-CR>"
	M.auto_update = opts.auto_update or false
	M.persistence_dir = opts.save_dir or "./problems/"

	vim.api.nvim_create_user_command("AlgoNvimUpdate", cmd.AlgoNvimUpdate, { nargs = "*" })
	vim.api.nvim_create_user_command("AlgoNvimOpen", cmd.AlgoNvimOpen, {})

	vim.keymap.set("n", M.keybind_launch, cmd.AlgoNvimOpen, { noremap = true, silent = true })

	if M.auto_update and #M.lang_table > 0 then
		vim.fn.jobstart(function()
			repo_utils.update_repo(M.lang_table)
		end)
	end
end

return M
