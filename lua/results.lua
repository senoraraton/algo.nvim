local M = {}

local Path = require("plenary.path")

local function create_floating_window(bufnr, content)
	local win_width = 80
	local win_height = 20
	local buf = vim.api.nvim_create_buf(false, true) -- Create a new buffer
	vim.api.nvim_buf_set_keymap(buf, "n", "<Esc>", ":q!<CR>", { noremap = true, silent = true })

	vim.api.nvim_buf_set_lines(buf, 0, -1, false, vim.split(content, "\n"))

	local opts = {
		relative = "editor",
		width = win_width,
		height = win_height,
		col = (vim.o.columns - win_width) / 2,
		row = (vim.o.lines - win_height) / 2,
		anchor = "NW",
		style = "minimal",
		border = "rounded",
	}

	vim.api.nvim_open_win(buf, true, opts)
end

local function writeCompletedTest(bufnr)
	local problem = vim.api.nvim_buf_get_var(bufnr, "problem")
	local filename = vim.api.nvim_buf_get_name(bufnr)
	print("Problem path: " .. problem.path)
	print("Filename: " .. filename)
end

function M.renderTests(checks_table)
	local resultString = ""
	local passed = true
	if checks_table["cmd_output"] ~= nil then
		resultString = "Compiler error:\n" .. checks_table["cmd_output"] .. "\n"
	else
		for index, result in ipairs(checks_table["test_results"]) do
			if result.isMatch then
				resultString = resultString .. string.format("Test %d: Success\n", index)
			else
				resultString = resultString .. string.format("Test %d: Error - %s\n", index, result.error)
				passed = false
			end
		end
	end
	local solution_bufnr = vim.api.nvim_get_current_buf()

	create_floating_window(0, resultString)

	if passed then
		writeCompletedTest(solution_bufnr)

		local answer = vim.fn.input("Would you like to save your work? (y?): ")
		if answer:lower() == "y" or answer:lower() == "yes" then
			local file_path = vim.api.nvim_buf_get_name(solution_bufnr)
			local path = Path:new(file_path)
			local filename = path:filename()
			local new_filename = filename:gsub("^tmp_", "")
			local new_path = Path:new(path:parent()):joinpath(new_filename)

			local success, err = os.rename(file_path, new_path:absolute())

			if not success then
				print("Failed to rename file. Error: " .. err)
			end
		end
	end
end

return M
