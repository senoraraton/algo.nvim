Algo.nvim:
All the excitement of grinding coding challenges, inside your comfy, cozy, and custom Neovim environment.

## Table of Contents

- [Setup](#setup)
- [Updating](#updating)
- [Supported Languages](#supported-languages)
- [Keybinds](#keybinds)
- [How to Contribute](#how-to-contribute)
  - [Building a Runner](#building-a-runner)
  - [Problem Related Submissions](#problem-related-submissions)
  - [Financial Contributions](#financial-contributions)
  - [Employment Inquiries](#employment-inquiries)
- [Todo](#todo)

## Setup

Requires Telescope - https://github.com/nvim-telescope/telescope.nvim

Install with your preffered plugin solution, then call algonvim.setup() in your configuration to initalize the plugin.
Setup takes an options table with the following options:

```lua
algonvim.setup({
    lang_table = { "c", "golang", "typescript", "lua" },
    -- At least one language is required in order to expose any problems.

    auto_update = false,
    --If false, you will need to manually call :AlgoNvimUpdate to initalize problem repositories.

    save_dir = "/home/user/algonvim-solutions",
    --Absolute path to your save directory. Set if you want to version control your solutions.

    -- See keybinds below for more information.
    keybind_launch = "<F12>",
    keybind_toggle_completed = "<A-CR>",
    keybind_select_default = "<CR>",
    keybind_select_all = "<S-CR>",
    keybind_select_random = "<C-CR>"
})
```

## Updating

Algo.nvim uses gitsubmodules to manage external problem repositories. If you do not enable auto_update, you must manually update the repositories by running `:AlgoNvimUpdate` for all repos, or `:AlgoNvimUpdate $LANG $LANG ...` for specific languages. These commands can be used to update your language repositories after the inital setup, and must be run again if you add languages to your language table.

## Supported Languages

Algo.nvim is an extensible framework designed to handle any language you can get to inter-op with Lua.

Currently supported languages include:
|Language | Repository |
|---------|---------------------------------------------|
|C | https://gitlab.com/senoraraton/algo.nvim.cpp

Planned Languages:
|Language | Repository |
|---------|------------------------------------------------------|
|Typescript | https://gitlab.com/senoraraton/algo.nvim.typescript|
|Lua| https://gitlab.com/senoraraton/algo.nvim.Lua |
|Golang| https://gitlab.com/senoraraton/algo.nvim.golang |

## Keybinds

Algo.nvim has a small set of keybinds that can be changed from the defaults in algonvim.setup().
| Variable | Description | Defaults |
| ------------------------------- | ------------------------------------------- | -------- |
| keybind_launch | Open the problem picker/Submit solution | `<F12>`
| keybind_select_default | Select an entry | `<CR>` |
| keybind_select_all | View all problems in selected entry | `<S-CR>` |
| keybind_select_random | Select a random problem from selected entry | `<C-CR>` |
| keybind_toggle_completed | Toggle visibility of completed problems | `<A-CR>` |

## How to Contribute.

Pull requests for core functionality(including runners) are accepted at https://gitlab.com/senoraraton/algo.nvim

### Building a runner

If you would like to extend the languages avaliable in Algo.nvim, a runner is need for that language.

Runners must conform to the following critera:

- Must be named "lang"-runner.lua and located in the ./lua/runners directory
- Must export a lua function named compileAndExecute with the paramaters(path, code).
  - path is the solution buffer local variable problem.path.
  - code is the contents of the buffer to be executed.
- Must return a lua table in the form of

```lua
local tests_results = {
    { isMatch = true, error = "" },
    { isMatch = false, error = "Failed due to mismatch" },
}
```

- Must have an associated submodule problems repository.

### Problem releated submissions

If you would like to contribute problems, solutions, learning resources, or bug reports on tests, please issue PRs to the problem repo directly.
_IMPORTANT_ If you utilize external resources, for inspiration, please include links to these resources in your PR. All problems
must be original creations, including description, tests, and solutions. Direct copying or close paraphrasing from external sources is not permitted.
Contributions that do not adhere to these guidelines will be rejected.

### Financial Contributions

If you would like to offer financial support, donation links are listed below.
https://ko-fi.com/senoraraton

### Employment inquiries

I am also currently seeking employment as a Backend/Dev Ops Engineer.
My portfolio can be found at https://scaleosarus.com

## Todo:

Backend:
Mark completed if completed
Offer to rename completed to problem name?
Check if problem_name exists and if so append #?

Write sample picker
Lua/Typescript/Golang interop
Write tutorial readme
Write testFile validator script
Function to clean tmp files
Database?!
profiling for compiles in c-runner?
reset button on problems page
