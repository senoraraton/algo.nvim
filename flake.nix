{
  description = "Algo.nvim!";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {nixpkgs, ...}: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfreePredicate = true;
    };
  in {
    devShells.x86_64-linux = {
      default = pkgs.mkShell {
        buildInputs = builtins.attrValues {
          inherit
            (pkgs)
            gcc
            lua
            nodejs_22
            ;
        };
        shellHook = ''
          export LUA_PATH="$LUA_PATH;$PWD/lua/?.lua;$PWD/lua/runners/?.lua"
        '';
      };
    };
  };
}
